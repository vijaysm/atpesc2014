/** @example HelloParMOAB.cpp \n
 *
 * \brief Read mesh into MOAB and resolve/exchange/report owned, shared and ghosted entities \n
 * <b>To run</b>: mpiexec -np 4 HelloMoabPar -f [filename] -g [ghost layers]\n
 *
 */

#include "moab/ProgOptions.hpp"
#include "moab/ParallelComm.hpp"
#include "MBParallelConventions.h"
#include "moab/Core.hpp"
#include "moab/CN.hpp"
#include "MBExamples.hpp"

using namespace moab;
using namespace std;

#ifndef MESH_DIR
#error Specify MESH_DIR for example to work with default inputs
#endif

string test_file_name = string(MESH_DIR) + string("/64bricks_512hex_256part.h5m");

void Print_Owned_and_Shared_Entities(int global_rank, int global_size, Range& owned, Range& shared);

int main(int argc, char **argv)
{
  ProgOptions opts;
  int global_rank, global_size, nghost=1;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank( MPI_COMM_WORLD, &global_rank );
  MPI_Comm_size( MPI_COMM_WORLD, &global_size );

  // Need option handling here for input filename
  opts.addOpt<string>(string("file,f"),
      std::string("Name of input file (default="+test_file_name+")"), &test_file_name);
  opts.addOpt<int>(string("nghost,g"),
      std::string("Number of ghost layers (default=1)"), &nghost);
  opts.parseCommandLine(argc, argv);

  // Get MOAB instance and read the file with the specified options
  Interface* mbImpl = new Core;

  // Get the ParallelComm instance
  ParallelComm* pcomm = new ParallelComm(mbImpl, MPI_COMM_WORLD);

  string options = (global_size>1 ? "PARALLEL=READ_PART;PARTITION=PARALLEL_PARTITION;PARALLEL_RESOLVE_SHARED_ENTS" : "");
  dbgprint( "Reading file " << test_file_name << "\n  with options: " << options << endl
              << " on " << global_size << " processors\n" );

  ErrorCode rval = mbImpl->load_file(test_file_name.c_str(), 0, options.c_str());CHKERR(rval,"Load file failed");

  Range shared_entities,owned_entities;
  if (global_size > 1) {

    // Get entities shared with all other processors
    rval = pcomm->get_shared_entities(-1, shared_entities);CHKERR(rval,"Getting shared entities failed");

    // Filter shared entities with not not_owned, which means owned
    rval = pcomm->filter_pstatus(shared_entities, PSTATUS_NOT_OWNED, PSTATUS_NOT, -1, &owned_entities);CHKERR(rval,"Filtering PSTATUS flag failed");

    // print the information   
    Print_Owned_and_Shared_Entities(global_rank, global_size, owned_entities, shared_entities);
    

    // Now exchange 1 layer of ghost elements, using vertices as bridge
    // (we could have done this as part of reading process, using the PARALLEL_GHOSTS read option)
    dbgprint( " \n\t <<< Exchanging " << nghost << " ghost layers >>> \n" );
    rval = pcomm->exchange_ghost_cells(3, // int ghost_dim,
                                      0, // int bridge_dim,
                                      nghost, //int num_layers,
                                      0, //int addl_ents,
                                      true // bool store_remote_handles);
                                      );CHKERR(rval,"Exchanging ghost cells between processors failed");

    // Repeat the reports, after ghost exchange
    shared_entities.clear();
    owned_entities.clear();
    rval = pcomm->get_shared_entities(-1, shared_entities);CHKERR(rval,"Getting shared entities failed");

    rval = pcomm->filter_pstatus(shared_entities, PSTATUS_NOT_OWNED, PSTATUS_NOT, -1, &owned_entities);CHKERR(rval,"Filtering PSTATUS flag failed");

    // print the information
    Print_Owned_and_Shared_Entities(global_rank, global_size, owned_entities, shared_entities);
  }
  else {
    rval = mbImpl->get_entities_by_handle(0, owned_entities, true);CHKERR(rval, "Getting entities failed");

    // print the information
    Print_Owned_and_Shared_Entities(global_rank, global_size, owned_entities, shared_entities);
  }
  
  // Only print on the root node
  if (global_rank == 0) {
  
    // single process run
    for (Range::iterator it = owned_entities.begin(); it != owned_entities.end(); it++) {
      if (MBVERTEX == mbImpl->type_from_handle(*it)) {
        Range adjs;
        rval = mbImpl->get_adjacencies(&(*it), 1, 3, false, adjs);CHKERR(rval,"Getting adjacencies failed");
        dbgprint ( "Vertex " << mbImpl->id_from_handle(*it) << " adjacencies:" << endl );
        adjs.print();
      }
      else if (mbImpl->type_from_handle(*it) < MBENTITYSET) {
        const EntityHandle *connect;
        int num_connect;
        rval = mbImpl->get_connectivity(*it, connect, num_connect);CHKERR(rval,"Getting connectivity failed");

        cout << CN::EntityTypeName(mbImpl->type_from_handle(*it)) << " " << mbImpl->id_from_handle(*it) << " vertex connectivity is: ";
        for (int i = 0; i < num_connect; i++)
          cout << mbImpl->id_from_handle(connect[i]) << " ";
        cout << endl;
      }
    }
  }

  // Cleanup MOAB objects
  delete mbImpl;

  MPI_Finalize();
  return 0;
}

void Print_Owned_and_Shared_Entities(int global_rank, int global_size, Range& owned, Range& shared)
{
  const int ndata=8;
  unsigned int nums[ndata] = {0}; // to store the owned and then shared entities per dimension
  for (int i = 0; i < ndata/2; i++)
    nums[i] = (int)owned.num_of_dimension(i);
  for (int i = ndata/2; i < ndata; i++)
    nums[i] = (int)shared.num_of_dimension(i-ndata/2);
  vector<int> rbuf(global_size*ndata, 0);
  MPI_Gather(nums, ndata, MPI_INT, &rbuf[0], ndata, MPI_INT, 0, MPI_COMM_WORLD);
  // Print the stats gathered:
  if (global_rank == 0) {
    for (int i = 0; i < global_size; i++)
      cout << " Owned (Shared) entities on processor (" << i << ") : " << rbuf[4*i] << "(" << rbuf[4*i+4] << ") verts, " <<
          rbuf[4*i + 1] << "(" << rbuf[4*i+5] << ") edges, " << rbuf[4*i + 2] << "(" << rbuf[4*i+6] << ") faces, " << 
          rbuf[4*i + 3] << "(" << rbuf[4*i+7] << ") elements" << endl;
  }
}

