/** @example SetsNTags.cpp
 * Description: Get the sets representing materials and Dirichlet/Neumann boundary conditions and list their contents.\n
 * This example shows how to get entity sets, and tags on those sets.
 *
 * To run: ./SetsNTags [meshfile]\n
 * (default values can run if users don't specify a mesh file)
 *
 * Note: This is a simple demo showing only serial queries
 */

#include "moab/Core.hpp"
#include "moab/ProgOptions.hpp"
#include "moab/Range.hpp"
#include "MBTagConventions.hpp"
#include "MBExamples.hpp"

using namespace moab;
using namespace std;

#ifndef MESH_DIR
#error Specify MESH_DIR for example to work with default inputs
#endif

string test_file_name = string(MESH_DIR) + string("/64bricks_512hex_256part.h5m");

// tag names for these conventional tags come from MBTagConventions.hpp
const char *tag_nms[] = {MATERIAL_SET_TAG_NAME, DIRICHLET_SET_TAG_NAME, NEUMANN_SET_TAG_NAME, PARALLEL_PARTITION_TAG_NAME};

int main(int argc, char **argv) {
  ErrorCode rval;

  ProgOptions opts;
  // Need option handling here for input filename
  opts.addOpt<string>(string("file,f"),
      std::string("Name of input file (default="+test_file_name+")"), &test_file_name);
  opts.parseCommandLine(argc, argv);

    // instantiate & load a file
  Interface *mbImpl = new Core();

  rval = mbImpl->load_file(test_file_name.c_str());if (MB_SUCCESS != rval) return 1;

  // define the tag handle
  Tag mtag;
  Range sets, set_ents;

    // loop over set types
  for (int i = 0; i < 4; i++) {
      // get the tag handle for this tag name; tag should already exist (it was created during file read)
    rval = mbImpl->tag_get_handle(tag_nms[i], 1, MB_TYPE_INTEGER, mtag);CHKERRNMPI(rval,"Getting tag handle ("+string(tag_nms[i])+") failed");

      // get all the sets having that tag (with any value for that tag)
    sets.clear();
    rval = mbImpl->get_entities_by_type_and_tag(0, MBENTITYSET, &mtag, NULL, 1, sets);CHKERRNMPI(rval,"Getting entity sets belonging to tag failed");

      // iterate over each set, getting the entities and printing them
    Range::iterator set_it;
    for (set_it = sets.begin(); set_it != sets.end(); set_it++)  {
        // get the id for this set
      int set_id;
      rval = mbImpl->tag_get_data(mtag, &(*set_it), 1, &set_id);CHKERRNMPI(rval,"Getting tag data failed");

        // get the entities in the set, recursively
      rval = mbImpl->get_entities_by_handle(*set_it, set_ents, true);CHKERRNMPI(rval,"Recursively getting entities failed");

      cout << tag_nms[i] << " " << set_id << " has " << set_ents.size() << " entities:" << endl;
      set_ents.print("   ");
      set_ents.clear();
    }
  }

  delete mbImpl;
  return 0;
}
