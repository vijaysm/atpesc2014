#
# Makefile for compiling MOAB and DMMoab examples for ATPESC 2014 workshop
#
# MOAB_DIR points to top-level install dir, below which MOAB's lib/ and include/ are located
MOAB_DIR := /gpfs/vesta-fs0/projects/FASTMath/ATPESC-2014/install/moab/4.7.0RC2/linux-rhel_6-ppc64-gcc-4.4.6
# PETSC_DIR points to top-level install dir, below which PETSc's conf/, lib/ and include/ are located
PETSC_DIR := /gpfs/vesta-fs0/projects/FASTMath/ATPESC-2014/install/petsc/3.5.1.1-moab/linux-rhel_6-ppc64-gcc-4.4.6
# prefix points to top-level examples dir, where the pre-compiled executables are available
prefix := /projects/FASTMath/ATPESC-2014/examples/moab

# MOAB includes
include ${MOAB_DIR}/lib/moab.make
include ${MOAB_DIR}/lib/iMesh-Defs.inc
# PETSc includes
include ${PETSC_DIR}/conf/variables

.SUFFIXES: .o .cpp .cxx

# MESH_DIR is the directory containing mesh files that come with MOAB source
MESH_DIR=${PWD}/input

MOAB_BASIC_EXAMPLES = HelloParMOAB GenLargeMesh SetsNTags
ADVANCED_EXAMPLES = MBSize LloydRelaxation
PETSC_EXAMPLES = DMMoabLaplacian

default: ${MOAB_BASIC_EXAMPLES} ${PETSC_EXAMPLES}
all: default
advanced: ${ADVANCED_EXAMPLES}

EXTRALIBS=-ldl

HelloParMOAB: HelloParMOAB.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

GenLargeMesh: GenLargeMesh.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

SetsNTags: SetsNTags.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

MBSize: MBSize.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

LloydRelaxation: LloydRelaxation.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

DMMoabLaplacian: DMMoabLaplacian.o ${PETSC_DIR}/include/petsc.h 
	-${CLINKER} -o  $@ $< ${PETSC_LIB}

install:
	cp HelloParMOAB    ${prefix}/
	cp GenLargeMesh    ${prefix}/
	cp SetsNTags       ${prefix}/
	cp DMMoabLaplacian ${prefix}/
	cp -rf input/      ${prefix}/

clean::
	rm -rf *.o *.mod *.h5m ${MOAB_BASIC_EXAMPLES} ${PETSC_EXAMPLES} ${ADVANCED_EXAMPLES}

.cpp.o:
	${MOAB_CXX} ${CXXFLAGS} ${MOAB_CXXFLAGS} ${MOAB_CPPFLAGS} ${MOAB_INCLUDES} -DMESH_DIR=\"${MESH_DIR}\" -c $<

.cxx.o:
	${CXX} -c ${CXX_FLAGS} ${CXXFLAGS} ${CCPPFLAGS} -c -Wall $< -o $@
