#include <iostream>

// Error routines for use with MOAB API
#define CHKERRNMPI(CODE, MSG)                              \
  do {                                                     \
    if (MB_SUCCESS != (CODE)) {                            \
      std::string errstr;  mbImpl->get_last_error(errstr); \
      std::cerr << errstr << std::endl;                    \
      std::cerr << MSG << std::endl;                       \
    }                                                      \
  } while(false)

// Error routines for use with MOAB API
#define CHKERR(CODE, MSG)                                  \
  do {                                                     \
    if (MB_SUCCESS != (CODE)) {                            \
      int mpierr=0;                                        \
      std::string errstr;  mbImpl->get_last_error(errstr); \
      std::cerr << errstr << std::endl;                    \
      std::cerr << MSG << std::endl;                       \
      MPI_Abort(MPI_COMM_WORLD,mpierr);                    \
    }                                                      \
  } while(false)

// Error routines for use with MPI API
#define MPICHKERR(CODE, MSG)                              \
  do {                                                    \
    if (0 != CODE) {                                      \
      int mpierr=0;                                       \
      std::cerr << MSG << std::endl;                      \
      MPI_Abort(MPI_COMM_WORLD,mpierr);                   \
    }                                                     \
  } while(false)

#define dbgprint(MSG)                                     \
  do {                                                    \
      if (!global_rank) std::cerr << MSG << std::endl;    \
  } while(false)

#define dbgprintall(MSG)                                      \
  do {                                                        \
      std::cerr << "[" << global_rank << "]: " << MSG << std::endl;  \
  } while(false)

