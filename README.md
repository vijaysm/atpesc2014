# README #

This repository hosts all the examples for the FastMath MOAB tutorial presented at the [ATPESC-2014 workshop](http://extremecomputingtraining.anl.gov).

### How do I get started? ###

* On ALCF Vesta machine

Clone the repository, and use the provided makefile with pre-installed PETSc and MOAB directories to compile and link executables. Just do "make" to create working basic examples and use the job submission system to run them.

* Locally on your machine

Clone the repository and modify the makefile to replace PETSC_DIR and MOAB_DIR with your local installation of these libraries. Then, as above, do "make" to create the examples and run with "mpiexec -n P ./example-executable --options"

### More information ###

* [SIGMA ATPESC-2014 webpage](http://sigma.mcs.anl.gov/atpesc2014)
* Any questions regarding examples can be sent to moab-dev _at_ mcs.anl.gov